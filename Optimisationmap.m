function [Color, bestpar] = Optimisationmap(B0, pH, step)
    metaname = ["Glutamate", "Myo-Inositol", "Creatine", "Phosphocreatine"];
    b1sweep = 0.1E-6:step*1E-6:10E-6;
    satsweep = 0.1:step:5;
    Color = zeros(length(satsweep), length(b1sweep), 4);
    dummys = 1;
    
    for sat = satsweep
        dummyb = 1;
        for b1 = b1sweep
            [frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym] = fivepoolmodel(b1, B0, sat, pH);
            
            ppmplaceb = find(abs(frequency-ppmb) < 0.001);
            ppmplacec = find(abs(frequency-ppmc) < 0.001);
            ppmplaced = find(abs(frequency-ppmd) < 0.001);
            ppmplacee = find(abs(frequency-ppme) < 0.001);
            
            Color(dummys, dummyb, 1) = MTRasym(ppmplaceb);
            Color(dummys, dummyb, 2) = MTRasym(ppmplacec);
            Color(dummys, dummyb, 3) = MTRasym(ppmplaced);
            Color(dummys, dummyb, 4) = MTRasym(ppmplacee);
            dummyb = dummyb + 1;
        end
        dummys = dummys + 1;
    end
    Colorb = Color(:,:,1)/max(max(Color(:,:,1)));
    Colorb = max(Colorb,0);
    Colorc = Color(:,:,2)/max(max(Color(:,:,2)));
    Colorc = max(Colorc,0);
    Colord = Color(:,:,3)/max(max(Color(:,:,3)));
    Colord = max(Colord,0);
    Colore = Color(:,:,4)/max(max(Color(:,:,4)));
    Colore = max(Colore,0);
    
    
    figure;
    subplot( 2, 2, 1 )
    imagesc(b1sweep*1E6, satsweep, Colorb);
    title(metaname(1) + ' @ ' + ppmb + ' ppm')
    xlabel('B1 (uT)')
    ylabel('Saturation Time (s)')
    colorbar
    set(gca,'YDir','normal');
    
    subplot( 2, 2, 2 )
    imagesc(b1sweep*1E6, satsweep, Colorc);
    title(metaname(2) + ' @ ' + ppmc + ' ppm')
    xlabel('B1 (uT)')
    ylabel('Saturation Time (s)')
    colorbar
    set(gca,'YDir','normal');
    
    subplot( 2, 2, 3 )
    imagesc(b1sweep*1E6, satsweep, Colord);
    title(metaname(3) + ' @ ' + ppmd + ' ppm')
    xlabel('B1 (uT)')
    ylabel('Saturation Time (s)')
    colorbar
    set(gca,'YDir','normal');
    
    subplot( 2, 2, 4 )
    imagesc(b1sweep*1E6, satsweep, Colore);
    title(metaname(4) + ' @ ' + ppme + ' ppm')
    xlabel('B1 (uT)')
    ylabel('Saturation Time (s)')
    colorbar
    set(gca,'YDir','normal');
    
    %find optimal parameters
    bestpar = zeros(2, 4);
    [max_numb,max_idxb] = max(Colorb(:));
    [max_numc,max_idxc] = max(Colorc(:));
    [max_numd,max_idxd] = max(Colord(:));
    [max_nume,max_idxe] = max(Colore(:));
    bestpar(1, 1) = satsweep(mod(max_idxb, size(Colorb,1)));
    bestpar(2, 1) = b1sweep(fix(max_idxb/size(Colorb,1))+1);
    bestpar(1, 2) = satsweep(mod(max_idxc, size(Colorc,1)));
    bestpar(2, 2) = b1sweep(fix(max_idxc/size(Colorc,1))+1);
    bestpar(1, 3) = satsweep(mod(max_idxd, size(Colord,1)));
    bestpar(2, 3) = b1sweep(fix(max_idxd/size(Colord,1))+1);
    bestpar(1, 4) = satsweep(mod(max_idxe, size(Colore,1)));
    bestpar(2, 4) = b1sweep(fix(max_idxe/size(Colore,1))+1);
end