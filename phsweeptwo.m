function [pHsweep, SBPsignal] = phsweeptwo(B1strength, B0, sattime)
    metaname = ["Glc", "MI", "Cr", "PCr", "GABA", "Tau", "Gln", "Glu", "mAmides"];
    SBPsignal = zeros(9, 11);
    pHsweep = 6.5:0.1:7.5;
    concentration = 0;
    
    for meta = 1:1:9
        dummy = 1;
        for ph = pHsweep
            [frequency, Z, ppm, MTRasym] = twopoolmodelxl(meta, concentration, B1strength, B0, sattime, ph);
            
            ppmplace = find(abs(frequency-ppm) < 0.001);
            SBPsignal(meta, dummy) = MTRasym(ppmplace);
            
            dummy = dummy + 1;
        end    
    end
    
    SBPsignalNorm = SBPsignal./sum(SBPsignal);
    bar(pHsweep, 100*SBPsignalNorm', 'stacked')
    title("Saturation time of " + sattime + " s, B1 = " + 1000000*B1strength + "uT")
    ylim([0, 100])
    xlim([6.45, 7.55])
    xlabel('pH')
    ylabel('Normalized contribution [%]')
    legend({metaname(1),metaname(2),metaname(3),metaname(4),metaname(5),metaname(6),metaname(7),metaname(8),metaname(9)},'Location','eastoutside')
    
end
