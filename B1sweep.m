function [b1sweep, maxvalsb, maxvalsc, maxvalsd, maxvalse] = B1sweep(B0, sattime, pH)
    metaname = ["Glutamate", "Myo-Inositol", "Creatine", "Phosphocreatine"];
    b1sweep = 0.1E-6:0.1E-6:10E-6;
    maxvalsb = zeros(1, length(b1sweep));
    maxvalsc = zeros(1, length(b1sweep));
    maxvalsd = zeros(1, length(b1sweep));
    maxvalse = zeros(1, length(b1sweep));
    dummy = 1;
    
    for b1 = b1sweep
        [frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym] = fivepoolmodel(b1, B0, sattime, pH);
        
        ppmplaceb = find(abs(frequency-ppmb) < 0.001);
        ppmplacec = find(abs(frequency-ppmc) < 0.001);
        ppmplaced = find(abs(frequency-ppmd) < 0.001);
        ppmplacee = find(abs(frequency-ppme) < 0.001);
        
        maxvalsb(dummy) = MTRasym(ppmplaceb);
        maxvalsc(dummy) = MTRasym(ppmplacec);
        maxvalsd(dummy) = MTRasym(ppmplaced);
        maxvalse(dummy) = MTRasym(ppmplacee);
        
        dummy = dummy + 1;
    end
    plot(b1sweep * 1E6, maxvalsb, b1sweep * 1E6, maxvalsc , b1sweep * 1E6, maxvalsd, b1sweep * 1E6, maxvalse)
    title("Saturation time of " + sattime + " s")
    grid on
    xlabel('B1(uT)')
    ylabel('M/M0')
    legend({metaname(1),metaname(2),metaname(3),metaname(4)},'Location','northeast')
end