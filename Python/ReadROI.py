import os  
import matplotlib.pyplot as plt
import xmlrec as xml
import numpy as np
import ROIpython as ROI
import scipy.io as sio
from scipy.interpolate import interp1d

def shiftfreq(frequencies):
    offsetrangewassr = 450
    
    wassrfile = "C:\\Users\\jaimy\\Documents\\Python Scripts\\Master Onderzoek\\ReadData\\Data\\CEST_Glu_0510204080mM_26062020\\OvernightCEST_Glu_0510204080mM_25062020_WASSR_50_1"
    
    imageswassr, general_infowassr, series_infowassr  = xml.read_xmlrec(wassrfile)

    imageArraywassr = np.squeeze(xml.to_nd_array( imageswassr, general_infowassr, series_infowassr ))

    centerImagewassr = imageArraywassr[:,:,imageArraywassr.shape[2] // 2,0]
    centerImagewassr = np.abs(centerImagewassr)
    
    maskmatrixwassr = sio.loadmat('maskarrayGlu2606.mat')
    maskswassr = maskmatrixwassr['masks']
    
    imageArrayCenterwassr = np.abs(imageArraywassr[:,:,imageArraywassr.shape[2] // 2,:])

    roiwassr = np.array([np.moveaxis(imageArrayCenterwassr,2,0) * maskswassr[:,:,i] for i in range(maskswassr.shape[2])])
    roiwassr = np.moveaxis(roiwassr,0,3)
    roiwassr = np.moveaxis(roiwassr,0,2)

    meandynamswassr = np.nanmean(np.true_divide(roiwassr.sum(0),(roiwassr!=0).sum(0)),0)

    meandynamswassr = meandynamswassr[1:,:]/meandynamswassr[0,:]

    frequencieswassr = np.linspace(-offsetrangewassr,offsetrangewassr,meandynamswassr.shape[0])
    
    
    return frequencies - frequencieswassr[np.argmin(meandynamswassr, axis=0)]


def plotROI(file, path, offsetrange):
        
    inputFileName = path + "\\" + file
    
    
    images, general_info, series_info  = xml.read_xmlrec(inputFileName)
    
    imageArray = np.squeeze(xml.to_nd_array( images, general_info, series_info ))  
        
    # Get center slice of the image
    centerImage = imageArray[:,:,imageArray.shape[2] // 2,0]
    centerImage = np.abs(centerImage)
    
    # Choose new ROI
    masks = ROI.chooseROI(centerImage)
    # Save new ROI
    sio.savemat('maskarrayGlu2606.mat', {'masks':masks})
    
    # Use old ROI 
    # maskmatrix = sio.loadmat('maskarrayCr2506.mat')
    # masks = maskmatrix['masks']
    
    imageArrayCenter = np.abs(imageArray[:,:,imageArray.shape[2] // 2,:])
    roi1mask = masks[:,:,0]
    
    roi = np.array([np.moveaxis(imageArrayCenter,2,0) * masks[:,:,i] for i in range(masks.shape[2])])
    roi1 = np.moveaxis(imageArrayCenter,2,0) * roi1mask
    roi = np.moveaxis(roi,0,3)
    roi = np.moveaxis(roi,0,2)
    
    meandynams = np.nanmean(np.true_divide(roi.sum(0),(roi!=0).sum(0)),0)
    
    meandynams = meandynams[1:,:]/meandynams[0,:]
    
    frequency = np.linspace(-offsetrange,offsetrange,meandynams.shape[0])
    frequencyintp = np.linspace(-offsetrange,offsetrange,301)
    
    frequencies = np.vstack((frequency,frequency,frequency,frequency,frequency)).transpose()
    
    frequencies = shiftfreq(frequencies)
     
    return frequencies, meandynams
    



def sweepfiles(path, offsetrange):
    # plt.figure()
    for fn in os.listdir(path):
        inputFileName = path + '\\' + fn
        
        print("Processing file " + fn)
        
        if (os.path.isfile(inputFileName) and ((fn.find('.XML') > -1) or (fn.find('.xml') > -1))):
            
            print("This is a suitable xml file: " + fn)
            
            frequencies, meandynams = plotROI(fn, path, offsetrange)
            
            for i in range(meandynams.shape[1]):
                savearray = np.vstack((frequencies[:,i],meandynams[:,i]))
                np.savetxt("Data\\OvernightCEST_Cr_510204080_25062020\\CSV\\"+fn[14:17] + fn[44:-8]+str(5*2**i)+"mM.csv", savearray.transpose(), delimiter=",")
                
                
                
                
    #         for i in range(meandynams.shape[1]):
    #             # meandynamsintp = interp1d(frequencies, meandynams[:,i], kind='slinear')
    #             # plt.plot(frequenciesintp/(42.577478518 * 7), meandynamsintp(frequenciesintp), label = i)
    #             plt.plot(frequencies[:,i]/(42.577478518 * 7), meandynams[:,i], label = i)
    #         plt.title(fn[14:17] + fn[44:-9])    
    #         plt.xlabel('Frequency [ppm]')
    #         plt.ylabel('Z/Z0')
    #         plt.legend(('5mM', '10mM', '20mM', '40mM', '80mM'))
    #         plt.grid()
    #         plt.xlim(5,-5)
    #         plt.savefig('Data\\OvernightCEST_Cr_510204080_25062020\\Figures\\' + fn[14:17] + fn[44:-9])
    #         plt.clf()
    # plt.close()            
            
            
#------------------------------------------------------------------------------        
#Define file, path and offsetrange

path = "C:\\Users\\jaimy\\Documents\\Python Scripts\\Master Onderzoek\\ReadData\\Data\\CEST_Glu_0510204080mM_26062020\\Sweepdata"
file = "OvernightCEST_Glu_0510204080mM_25062020_CEST_4uT_500ms_11_1"
offsetrange = 1500

#------------------------------------------------------------------------------
#Only use the imports for sweepfiles
# import matplotlib
# matplotlib.use('Agg')

# sweepfiles(path, offsetrange)



frequencies, meandynams = plotROI(file, path, offsetrange)

plt.figure()
for i in range(meandynams.shape[1]):
    # meandynamsintp = interp1d(frequencies, meandynams[:,i], kind='slinear')
    # plt.plot(frequenciesintp/(42.577478518 * 7), meandynamsintp(frequenciesintp), label = i)
    plt.plot(frequencies[:,i]/(42.577478518 * 7), meandynams[:,i], label = i)
plt.title(file[14:17] + file[44:-5])    
plt.xlabel('Frequency [ppm]')
plt.ylabel('Z/Z0')
plt.legend(('5mM', '10mM', '20mM', '40mM', '80mM'))
plt.grid()
plt.xlim(5,-5)
plt.show()