#Imports necessary to run this program

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares
import CESTfunctions as cest
from metabolites import cr

#-----------------------------------------------------------------------------
#Functions

def _1lorentzian(x, amp1, cen1, wid1):
    return amp1/(1+4*((x-cen1)/(wid1))**2) 
                
def _2lorentzian(p, x, y):
    return (p[0]/(1+4*((x-p[1])/(p[2]))**2)) + (p[3]/(1+4*((x-p[4])/(p[5]))**2)) - y

def _3lorentzian(p, x, y):
    return (p[0]/(1+4*((x-p[1])/(p[2]))**2)) + (p[3]/(1+4*((x-p[4])/(p[5]))**2)) + (p[6]/(1+4*((x-p[7])/(p[8]))**2)) - y
                
#-----------------------------------------------------------------------------
#Run simulations from CESTfunctions
    
amountpools = 2

frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym = cest.simdata(
    cr, 1, 2, 2.95, 7, 1.5, 7, amountpools, 0) 

# frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym = cest.simdata(
#     2, 1, 2, 1.981, 7, 0.54, 7, amountpools) #creatine

#-----------------------------------------------------------------------------
#Make arrays and first guesses

x_array = frequency
y_array_2lor = 1 - Z

cenidx2 = np.where(np.abs(frequency - ppmb) < 0.01)
cenidx2 = cenidx2[0]

# cenidx3 = np.where(np.abs(frequency - ppmc) < 0.01)
# cenidx3 = cenidx3[0]

amp1 = 0.9987
cen1 = 0
wid1 = 3
amp2 = MTRasym[cenidx2[0]]
cen2 = ppmb
wid2 = 1
# amp3 = MTRasym[cenidx3[0]]
# cen3 = ppmc
# wid3 = 1

yfit = y_array_2lor

p = np.array([amp1, cen1, wid1, amp2, cen2, wid2])

boundsmin = [0, 0-1E-12, 0, 0, ppmb-1E-12, 0]
boundsmax = [1, 0+1E-12, 5, 0.55, ppmb+1E-12, 3]

#-----------------------------------------------------------------------------
#Optimalisation

counter = 0

while any(t > 0.01 for t in yfit):
    counter += 1
    if counter > 20: #emergency break to avoid infinite loop
        break
    
    popt_2lor = least_squares(_2lorentzian, p,
                                    bounds=(boundsmin, boundsmax),
                                    args=(x_array, y_array_2lor))
    
    pars_1 = popt_2lor.x[0:3]
    pars_2 = popt_2lor.x[3:6]
    lor_peak_1 = _1lorentzian(x_array, *pars_1)
    lor_peak_2 = _1lorentzian(x_array, *pars_2)
    
    p[0], p[1], p[2], p[3], p[4], p[5] = popt_2lor.x[0:6]
    
    yfit = popt_2lor.fun

#-----------------------------------------------------------------------------
#Plot figure
    
plt.figure()
plt.plot(x_array, y_array_2lor, label = "1 - Z")
plt.plot(x_array, lor_peak_1 + lor_peak_2, label = "Lorentzian fit")
# plt.plot(x_array, lor_peak_1, label = "Water Fit")
# plt.plot(x_array, lor_peak_2, label = "Metabolite Fit")
plt.plot(x_array, lor_peak_1 + lor_peak_2 - y_array_2lor, label = "Residual")
# plt.plot(x_array, MTRasym, label = "MTRasym")
plt.legend(loc='upper right')
plt.xlim([10, -10])
plt.grid()
plt.xlabel("B1 [$\mu$T]")
plt.ylabel("1-Z")
plt.show()