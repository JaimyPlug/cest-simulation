import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares
from metabolites import cr, glu, pcr, mi
import CESTfunctions as cest
from dataclasses import dataclass
import Zaissfit2 as zf2


xZspec = np.linspace(-10, 10, 1000)

metabolite      = cr
metabolitec     = glu
metabolited     = pcr


B0 = 7
pH = 7
B1 = 0.5
sattime = 1.5


file = "C:\\Users\\jaimy\\Documents\\Python Scripts\\Master Onderzoek\\ReadData\\Data\\OvernightCEST_Cr_510204080_25062020\\CSV\\Cr_2p95uT_1500ms_10mM.csv"

frequency, Z = np.loadtxt(file, delimiter=',', usecols=(0, 1), unpack=True)
frequency = frequency/(42.577478518 * 7)


Zfit = zf2.zaissfit(metabolite, B1, sattime)

plt.figure()
plt.plot(frequency,Z, label="real")
plt.plot(xZspec, Zfit, label="fit")
plt.xlim(10, -10)
plt.grid()
plt.legend()
plt.show()