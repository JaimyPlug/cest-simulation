import os  
import numpy as np
import matplotlib.pyplot as plt
from metabolites import cr, glu

sattimes = ["500", "1000", "1500", "2000", "2500"]
B1s = ["0p5", "1", "1p5", "2", "2p25", "2p5", "2p95", "3p5", "4"]

def maxvals(b1, sat, concentration):
    file = path + "\\" + metabolite.shortname + "_" + B1s[b1] + "uT_" + sattimes[sat] + "ms_" + concentration + ".csv"
    
    frequency, dynamics = np.loadtxt(file, delimiter=',', usecols=(0, 1), unpack=True)
    frequency = frequency/(42.577478518 * 7)
                
    ppmindex = np.where(np.abs(frequency-metabolite.ppm) < (frequency[1]-frequency[0])/2 )[0][0]
    
    minindex = np.min(dynamics[ppmindex-2:ppmindex+3])
    
    return 1 - minindex


def Optimap(concentration, path):  
    
    optimisationmatrix = np.zeros((len(sattimes), len(B1s)))
    
    for i in range(len(B1s)):
        for j in range(len(sattimes)):
            if i > 7:
                if j > 0:
                    maxvalue = 0
                else: 
                    maxvalue = maxvals(i, j, concentration)        
            
            elif i > 6:
                if j > 1:
                    maxvalue = 0
                else:
                    maxvalue = maxvals(i,j, concentration)
                    
            elif i > 5:
                if j > 2:
                    maxvalue = 0
                else:
                    maxvalue = maxvals(i,j, concentration)
                    
            elif i > 4:
                if j > 3:
                    maxvalue = 0
                else:
                    maxvalue = maxvals(i,j, concentration)
                    
            else:
                maxvalue = maxvals(i,j, concentration)
               
            optimisationmatrix[j,i] = maxvalue
      
    optimisationmatrix = np.flip(optimisationmatrix, axis = 0)        
    optimisationmatrix = optimisationmatrix/np.max(optimisationmatrix) 
    optimisationmatrix = [(i > 0) * i for i in optimisationmatrix] 
    
    plt.figure()
    plt.imshow(optimisationmatrix, extent= [0.5, 4, 0.25, 2.75])
    plt.xlabel('B1 [uT]')
    plt.ylabel('saturation time [s]')
    plt.title(metabolite.shortname + ' ' + concentration)
    plt.colorbar()
    # plt.savefig('Data\\' + metabolite.name + '\\Figures\\' + metabolite.shortname + ' ' + concentration + ' optimap')


metabolite = cr

path = "C:\\Users\\jaimy\\Documents\\Python Scripts\\Master Onderzoek\\ReadData\\Data\\" + metabolite.name + "\\CSV"
# concentration = "80mM"

# Optimap(concentration, path)
for i in ["5mM", "10mM", "20mM", "40mM", "80mM"]:
    Optimap(i, path)
