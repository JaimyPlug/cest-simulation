import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as sl
from scipy.integrate import trapz
from metabolites import cr, glu, pcr, mi

#-----------------------------------------------------------------------------
#MT effect

T1mt = 1
T2mt = 8.7E-6

MBmt = 0.2
m0mt = MBmt

Cmt = 66

kmt = 1/T1mt + Cmt


def MTeffect(A, m0, b, omega, omega1):  
    
    theta = np.linspace(0, np.pi/2, 1000)
    
    gmt = trapz(np.sin(theta) * np.sqrt(2/np.pi) * T2mt/(np.abs(3*(np.cos(theta))**2-1)) * np.exp(-2*((2*np.pi*omega*T2mt)/(np.abs(3*(np.cos(theta))**2-1)))**2), theta)
    
    MTvertical = np.zeros((len(A), 1))
    MTvertical[3, 0] = Cmt
    
    MThorizontal = np.zeros((1, len(A)+1))
    MThorizontal[0,3] = Cmt
    MThorizontal[0,-1] = -kmt - omega1**2 * np.pi * gmt
    
    A = np.concatenate((A, MTvertical), axis = 1)
    A = np.concatenate((A, MThorizontal), axis = 0)
    
    b = np.concatenate((b, [[m0mt/T1mt]]), axis = 0)
    
    m0 = np.concatenate((m0, [[m0mt]]), axis = 0)
    
    return A, b, m0

#-----------------------------------------------------------------------------
#Functions

def lorentzian( x, x0, a, gam ):
    return a * gam**2 / ( gam**2 + ( x - x0 )**2)


def twopoolmodel(metabolite, B1, B0, sattime, pH, MT):
    #Read data from chosen metabolite
  
    ppm = metabolite.ppm
    molarmass = metabolite.molarmass
    T2b = metabolite.T2
    k0 = metabolite.k0
    kb = metabolite.kb
    GM = metabolite.concentration  
    
    #-------------------------------------------------------------------------
    #First calculations
    
    omegaA = 0
    omegaB = 2 * np.pi * 42.577478518 * B0 * ppm
    T1a = 1.2
    T2a = 0.04
    T1b = 1
    
    #Calculation of exchange rate
    Cb = k0 + kb * 10 ** (pH - 14)  
    MB = molarmass * GM
    Ca = MB * Cb
    
    k1a = 1/T1a + Ca
    k1b = 1/T1b + Cb
    k2a = 1/T2a + Ca
    k2b = 1/T2b + Cb
    
    omega1 = 2 * np.pi * B1 * 42.577478518
    
    m0a = 1
    m0b = MB
    
    #-------------------------------------------------------------------------
    #Solve BM equations
    
    maxfreq = 42.577478518 * B0 * 10
    frequency = np.linspace(-maxfreq, maxfreq, 1000)
    frequency = 2 * np.pi * frequency
    
    Z = np.zeros_like(frequency)
    dummy = 0
    
    for omega in frequency:
        A = np.array([[-k2a, omegaA - omega, 0, Cb, 0, 0],
                      [omega - omegaA, -k2a, omega1, 0, Cb, 0],
                      [0, -omega1, -k1a, 0, 0, Cb],
                      [Ca, 0, 0, -k2b, omegaB - omega, 0],
                      [0, Ca, 0, omega - omegaB, -k2b, omega1],
                      [0, 0, Ca, 0, -omega1, -k1b]
                      ])
        
        b = np.array([[0], [0], [m0a/T1a], [0], [0], [m0b/T1b]])
    
        m0 = np.array([[0], [0], [m0a], [0], [0], [m0b]])
        
        if MT:
            A, b, m0 = MTeffect(A, m0, b, omega, omega1)
        
        x = np.linalg.solve(A,b)
        
        M = np.dot(sl.expm(A * sattime),(m0 + x)) - x
        
        # if omega == 0:
        #     print(M)
        # elif np.abs(omega - omegaB) < 0.0001:
        #     print(M)
        
        Z[dummy] = M[2] + M[5]
        
        if MT:
            Z[dummy] += M[6]
        
        dummy += 1
    
    #calculate MTRasym    
    S = np.flip(Z)
    MTRasym = (S - Z)
    length = len(Z)
    MTRasym[0:int((length-1)/2)] = 0
    
    frequency = frequency/(2 * np.pi * 42.577478518 * B0)
    
    maxindexlist = np.where(MTRasym == np.max(MTRasym))
    maxindex = maxindexlist[0]
    maxppm = frequency[maxindex[0]]  
    
    # print("Maximum value in MTRasym is at {0:.2f} ppm".format(maxppm)) 
    
    return(frequency, Z, ppm, MTRasym)


def threepoolmodel(metabolite, metabolitec, B1, B0, sattime, pH):
    #Read data from chosen metabolite
  
    ppmb = metabolite.ppm
    molarmassb = metabolite.molarmass
    T2b = metabolite.T2
    k0b = metabolite.k0
    kbb = metabolite.kb
    GMb = metabolite.concentration

    ppmc = metabolitec.ppm
    molarmassc = metabolitec.molarmass
    T2c = metabolitec.T2
    k0c = metabolitec.k0
    kbc = metabolitec.kb
    GMc = metabolitec.concentration   
    
    #-------------------------------------------------------------------------
    #First calculations
    
    omegaA = 0
    omegaB = 2 * np.pi * 42.577478518 * B0 * ppmb
    omegaC = 2 * np.pi * 42.577478518 * B0 * ppmc
    T1a = 1.2
    T2a = 0.04
    T1b = 1
    T1c = 1
    
    #Calculation of exchange rate
    Cb = k0b + kbb * 10 ** (pH - 14)
    MBb = molarmassb * GMb
    Cab = MBb * Cb
    
    Cc = k0c + kbc * 10 ** (pH - 14)
    MBc = molarmassc * GMc
    Cac = MBc * Cc
    
    Ca = Cab + Cac
    
    k1a = 1/T1a + Ca
    k1b = 1/T1b + Cb
    k1c = 1/T1c + Cc
    k2a = 1/T2a + Ca
    k2b = 1/T2b + Cb
    k2c = 1/T2c + Cc
    
    omega1 = 2 * np.pi * B1 * 42.577478518E6
    
    m0a = 1
    m0b = MBb
    m0c = MBc
    
    #-------------------------------------------------------------------------
    #Solve BM equations
    
    maxfreq = 42.577478518 * B0 * 10
    frequency = np.linspace(-maxfreq, maxfreq, 1001)
    frequency = 2 * np.pi * frequency
    
    m0 = np.array([[0], [0], [m0a], [0], [0], [m0b], [0], [0], [m0c]])
    
    Z = np.zeros(1001)
    dummy = 0
    
    for omega in frequency:
        A = np.array([[-k2a, omegaA - omega, 0, Cb, 0, 0, Cc, 0, 0],
                      [omega - omegaA, -k2a, omega1, 0, Cb, 0, 0, Cc, 0],
                      [0, -omega1, -k1a, 0, 0, Cb, 0, 0, Cc],
                      [Cab, 0, 0, -k2b, omegaB - omega, 0, 0, 0, 0],
                      [0, Cab, 0, omega - omegaB, -k2b, omega1, 0, 0, 0],
                      [0, 0, Cab, 0, -omega1, -k1b, 0, 0, 0],
                      [Cac, 0, 0, 0, 0, 0, -k2c, omegaC - omega, 0],
                      [0, Cac, 0, 0, 0, 0, omega - omegaC, -k2c, omega1],
                      [0, 0, Cac, 0, 0, 0, 0, -omega1, -k1c]
                      ])
        
        b = np.array([[0], [0], [m0a/T1a], [0], [0], [m0b/T1b], [0], [0], [m0c/T1c]])
        
        x = np.linalg.solve(A,b)
        
        M = np.dot(sl.expm(A * sattime),(m0 + x)) - x
        
        # if omega == 0:
        #     print(M)
        # elif np.abs(omega - omegaB) < 0.0001:
        #     print(M)
        
        Z[dummy] = M[2] + M[5] + M[8]
        
        dummy += 1
    
    #calculate MTRasym    
    S = np.flip(Z)
    MTRasym = (S - Z)
    length = len(Z)
    MTRasym[0:int((length-1)/2)] = 0
    
    frequency = frequency/(2 * np.pi * 42.577478518 * B0)
    
    maxindexlist = np.where(MTRasym == np.max(MTRasym))
    maxindex = maxindexlist[0]
    maxppm = frequency[maxindex[0]]  
    
    # print("Maximum value in MTRasym is at {0:.2f} ppm".format(maxppm)) 
    
    return(frequency, Z, ppmb, ppmc, MTRasym)


def fourpoolmodel(metabolite, metabolitec, metabolited, B1, B0, sattime, pH):
    #Read data from chosen metabolite
  
    ppmb = metabolite.ppm
    molarmassb = metabolite.molarmass
    T2b = metabolite.T2
    k0b = metabolite.k0
    kbb = metabolite.kb
    GMb = metabolite.concentration

    ppmc = metabolitec.ppm
    molarmassc = metabolitec.molarmass
    T2c = metabolitec.T2
    k0c = metabolitec.k0
    kbc = metabolitec.kb
    GMc = metabolitec.concentration    
    
    ppmd = metabolited.ppm
    molarmassd = metabolited.molarmass
    T2d = metabolited.T2
    k0d = metabolited.k0
    kbd = metabolited.kb
    GMd = metabolited.concentration 
    
    #-------------------------------------------------------------------------
    #First calculations
    
    omegaA = 0
    omegaB = 2 * np.pi * 42.577478518 * B0 * ppmb
    omegaC = 2 * np.pi * 42.577478518 * B0 * ppmc
    omegaD = 2 * np.pi * 42.577478518 * B0 * ppmd
    T1a = 1.2
    T2a = 0.04
    T1b = 1
    T1c = 1
    T1d = 1
    
    #Calculation of exchange rate
    Cb = k0b + kbb * 10 ** (pH - 14)
    MBb = molarmassb * GMb
    Cab = MBb * Cb
    
    Cc = k0c + kbc * 10 ** (pH - 14)
    MBc = molarmassc * GMc
    Cac = MBc * Cc
    
    Cd = k0d + kbd * 10 ** (pH - 14)
    MBd = molarmassd * GMd
    Cad = MBd * Cd
    
    Ca = Cab + Cac + Cad
    
    k1a = 1/T1a + Ca
    k1b = 1/T1b + Cb
    k1c = 1/T1c + Cc
    k1d = 1/T1d + Cd
    k2a = 1/T2a + Ca
    k2b = 1/T2b + Cb
    k2c = 1/T2c + Cc
    k2d = 1/T2d + Cd
    
    omega1 = 2 * np.pi * B1 * 42.577478518E6
    
    m0a = 1
    m0b = MBb
    m0c = MBc
    m0d = MBd
    
    #-------------------------------------------------------------------------
    #Solve BM equations
    
    maxfreq = 42.577478518 * B0 * 10
    frequency = np.linspace(-maxfreq, maxfreq, 1001)
    frequency = 2 * np.pi * frequency
    
    m0 = np.array([[0], [0], [m0a], [0], [0], [m0b], [0], [0], [m0c], [0], [0], [m0d]])
    
    Z = np.zeros(1001)
    dummy = 0
    
    for omega in frequency:
        A = np.array([[-k2a, omegaA - omega, 0, Cb, 0, 0, Cc, 0, 0, Cd, 0, 0],
                      [omega - omegaA, -k2a, omega1, 0, Cb, 0, 0, Cc, 0, 0, Cd, 0],
                      [0, -omega1, -k1a, 0, 0, Cb, 0, 0, Cc, 0, 0, Cd],
                      [Cab, 0, 0, -k2b, omegaB - omega, 0, 0, 0, 0, 0, 0, 0],
                      [0, Cab, 0, omega - omegaB, -k2b, omega1, 0, 0, 0, 0, 0, 0],
                      [0, 0, Cab, 0, -omega1, -k1b, 0, 0, 0, 0, 0, 0],
                      [Cac, 0, 0, 0, 0, 0, -k2c, omegaC - omega, 0, 0, 0, 0],
                      [0, Cac, 0, 0, 0, 0, omega - omegaC, -k2c, omega1, 0, 0, 0],
                      [0, 0, Cac, 0, 0, 0, 0, -omega1, -k1c, 0, 0, 0],
                      [Cad, 0, 0, 0, 0, 0, 0, 0, 0, -k2d, omegaD - omega, 0],
                      [0, Cad, 0, 0, 0, 0, 0, 0, 0, omega - omegaD, -k2d, omega1],
                      [0, 0, Cad, 0, 0, 0, 0, 0, 0, 0, -omega1, -k1d]
                      ])
        
        b = np.array([[0], [0], [m0a/T1a], [0], [0], [m0b/T1b], [0], [0], [m0c/T1c], [0], [0], [m0d/T1d]])
        
        x = np.linalg.solve(A,b)
        
        M = np.dot(sl.expm(A * sattime),(m0 + x)) - x
        
        # if omega == 0:
        #     print(M)
        # elif np.abs(omega - omegaB) < 0.0001:
        #     print(M)
        
        Z[dummy] = M[2] + M[5] + M[8] + M[11]
        
        dummy += 1
    
    #calculate MTRasym    
    S = np.flip(Z)
    MTRasym = (S - Z)
    length = len(Z)
    MTRasym[0:int((length-1)/2)] = 0
    
    frequency = frequency/(2 * np.pi * 42.577478518 * B0)
    
    maxindexlist = np.where(MTRasym == np.max(MTRasym))
    maxindex = maxindexlist[0]
    maxppm = frequency[maxindex[0]]  
    
    # print("Maximum value in MTRasym is at {0:.2f} ppm".format(maxppm)) 
    
    return(frequency, Z, ppmb, ppmc, ppmd, MTRasym)


def fivepoolmodel(B1, B0, sattime, pH):
    #Read data from chosen metabolite
    
    metabolite = 0 #glutamate
    metabolitec = 2 #creatine
    metabolited = 1 #MI
    metabolitee = 3 #pcreatine
    
    
    ppmb = cr.ppm
    molarmassb = cr.molarmass
    T2b = cr.T2
    k0b = cr.k0
    kbb = cr.kb
    GMb = cr.concentration

    ppmc = glu.ppm
    molarmassc = glu.molarmass
    T2c = glu.T2
    k0c = glu.k0
    kbc = glu.kb
    GMc = glu.concentration    
    
    ppmd = pcr.ppm
    molarmassd = pcr.molarmass
    T2d = pcr.T2
    k0d = pcr.k0
    kbd = pcr.kb
    GMd = pcr.concentration 
    
    ppme = mi.ppm
    molarmasse = mi.molarmass
    T2e = mi.T2
    k0e = mi.k0
    kbe = mi.kb
    GMe = mi.concentration
    
    #-------------------------------------------------------------------------
    #First calculations
    
    omegaA = 0
    omegaB = 2 * np.pi * 42.577478518 * B0 * ppmb
    omegaC = 2 * np.pi * 42.577478518 * B0 * ppmc
    omegaD = 2 * np.pi * 42.577478518 * B0 * ppmd
    omegaE = 2 * np.pi * 42.577478518 * B0 * ppme
    T1a = 1.2
    T2a = 0.04
    T1b = 1
    T1c = 1
    T1d = 1
    T1e = 1
    
    #Calculation of exchange rate
    Cb = k0b + kbb * 10 ** (pH - 14)
    MBb = molarmassb * GMb
    Cab = MBb * Cb
    
    Cc = k0c + kbc * 10 ** (pH - 14)
    MBc = molarmassc * GMc
    Cac = MBc * Cc
    
    Cd = k0d + kbd * 10 ** (pH - 14)
    MBd = molarmassd * GMd
    Cad = MBd * Cd
    
    Ce = k0e + kbe * 10 ** (pH - 14)
    MBe = molarmasse * GMe
    Cae = MBe * Ce
    
    Ca = Cab + Cac + Cad + Cae
    
    k1a = 1/T1a + Ca
    k1b = 1/T1b + Cb
    k1c = 1/T1c + Cc
    k1d = 1/T1d + Cd
    k1e = 1/T1e + Ce
    k2a = 1/T2a + Ca
    k2b = 1/T2b + Cb
    k2c = 1/T2c + Cc
    k2d = 1/T2d + Cd
    k2e = 1/T2e + Ce
    
    omega1 = 2 * np.pi * B1 * 42.577478518E6
    
    m0a = 1
    m0b = MBb
    m0c = MBc
    m0d = MBd
    m0e = MBe
    
    #-------------------------------------------------------------------------
    #Solve BM equations
    
    maxfreq = 42.577478518 * B0 * 10
    frequency = np.linspace(-maxfreq, maxfreq, 1000)
    frequency = 2 * np.pi * frequency
    
    m0 = np.array([[0], [0], [m0a], [0], [0], [m0b], [0], [0], [m0c], [0], [0], [m0d], [0], [0], [m0e]])
    
    Z = np.zeros(1000)
    dummy = 0
    
    for omega in frequency:
        A = np.array([[-k2a, omegaA - omega, 0, Cb, 0, 0, Cc, 0, 0, Cd, 0, 0, Ce, 0, 0],
                      [omega - omegaA, -k2a, omega1, 0, Cb, 0, 0, Cc, 0, 0, Cd, 0, 0, Ce, 0],
                      [0, -omega1, -k1a, 0, 0, Cb, 0, 0, Cc, 0, 0, Cd, 0, 0, Ce],
                      [Cab, 0, 0, -k2b, omegaB - omega, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                      [0, Cab, 0, omega - omegaB, -k2b, omega1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                      [0, 0, Cab, 0, -omega1, -k1b, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                      [Cac, 0, 0, 0, 0, 0, -k2c, omegaC - omega, 0, 0, 0, 0, 0, 0, 0],
                      [0, Cac, 0, 0, 0, 0, omega - omegaC, -k2c, omega1, 0, 0, 0, 0, 0, 0],
                      [0, 0, Cac, 0, 0, 0, 0, -omega1, -k1c, 0, 0, 0, 0, 0, 0],
                      [Cad, 0, 0, 0, 0, 0, 0, 0, 0, -k2d, omegaD - omega, 0, 0, 0, 0],
                      [0, Cad, 0, 0, 0, 0, 0, 0, 0, omega - omegaD, -k2d, omega1, 0, 0, 0],
                      [0, 0, Cad, 0, 0, 0, 0, 0, 0, 0, -omega1, -k1d, 0, 0, 0],
                      [Cae, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -k2e, omegaE - omega, 0],
                      [0, Cae, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, omega - omegaE, -k2e, omega1],
                      [0, 0, Cae, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -omega1, -k1e]
                      ])
        
        b = np.array([[0], [0], [m0a/T1a], [0], [0], [m0b/T1b], [0], [0], [m0c/T1c], [0], [0], [m0d/T1d], [0], [0], [m0e/T1e]])
        
        x = np.linalg.solve(A,b)
        
        M = np.dot(sl.expm(A * sattime),(m0 + x)) - x
        
        # if omega == 0:
        #     print(M)
        # elif np.abs(omega - omegaB) < 0.0001:
        #     print(M)
        
        Z[dummy] = M[2] + M[5] + M[8] + M[11] + M[14]
        
        dummy += 1
    
    #calculate MTRasym    
    S = np.flip(Z)
    MTRasym = (S - Z)
    length = len(Z)
    MTRasym[0:int((length-1)/2)] = 0
    
    frequency = frequency/(2 * np.pi * 42.577478518 * B0)
    
    maxindexlist = np.where(MTRasym == np.max(MTRasym))
    maxindex = maxindexlist[0]
    maxppm = frequency[maxindex[0]]  
    
    # print("Maximum value in MTRasym is at {0:.2f} ppm".format(maxppm)) 
    
    return(frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym)



def optimap(metabolite, b1sweep, B0, satsweep, pH, MT):
    matrix = np.zeros((len(satsweep), len(b1sweep)))
    dummyb1 = 0
    for b1 in b1sweep:
        dummysat = 0
        for sat in satsweep:
            frequency, Z, ppmb, MTRasym = twopoolmodel(
                metabolite, b1, B0, sat, pH, MT)
            
            signalplace = np.where(np.abs(frequency - ppmb) < 0.001)
            signalplace = signalplace[0]
            signal = MTRasym[signalplace[0]]
            
            matrix[dummysat, dummyb1] = signal
            
            dummysat += 1
        
        dummyb1 += 1
        
        print("{} of {}".format(dummyb1, len(b1sweep)))
    
    return(matrix)



def contribution(B1, B0, sattime, pH, MT):
    FOS = np.zeros((2, 225))
    
    dummy = 0
    for meta in [0,2]:
        frequency, Z, ppmb, MTRasym = twopoolmodel(
            meta, B1, B0, sattime, pH, MT)
        FOS[dummy,:] = MTRasym[525:750]
        
        dummy += 1
    
    FOS = [(i > 0) * i for i in FOS] #make negative values equal 0
    FOSnorm = FOS/(np.sum(FOS, axis = 0))
    
    return(frequency, FOSnorm, FOS)
    

def simdata(metabolite, metabolitec, metabolited, B1, B0, sattime, pH, amountpools, MT):
    ppmc = 0
    ppmd = 0
    ppme = 0
    
    if amountpools == 2:
        frequency, Z, ppmb, MTRasym = twopoolmodel(
            metabolite, B1, B0, sattime, pH, MT)
    elif amountpools == 3:
        frequency, Z, ppmb, ppmc, MTRasym = threepoolmodel(
            metabolite, metabolitec, B1, B0, sattime, pH)
    elif amountpools == 4:
        frequency, Z, ppmb, ppmc, ppmd, MTRasym = fourpoolmodel(
            metabolite, metabolitec, metabolited, B1, B0, sattime, pH)
    elif amountpools == 5:
        frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym = fivepoolmodel(
            B1, B0, sattime, pH)
    else:
        print('AmountPools must be 2, 3, 4 or 5.')
    
    return(frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym)

    
    
