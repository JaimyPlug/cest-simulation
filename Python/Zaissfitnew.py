#Imports necessary to run this program

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares
from metabolites import cr, glu, pcr, mi
import CESTfunctions as cest
from dataclasses import dataclass
import Zaissfit2 as zf2

@dataclass
class mg:
    ppm: float
    molarmass: float
    T2: float
    k0: int
    kb: int
    concentration: float

#-----------------------------------------------------------------------------
#Functions

def Rex_Lorentz(da,w1,db,fb,kb,r2b):
    ka=kb*fb
     
    REXMAX = ka*w1**2/(da**2+w1**2)*((da-db)**2+(da**2+w1**2)*r2b/kb + r2b*(kb+r2b))
    GAMMA = 2*np.sqrt((kb+r2b)/kb*w1**2+(kb+r2b)**2)
    return REXMAX/((GAMMA/2)**2+db**2)


def zaissfit(p, x, y):
    
    ppm = p[0]#metabolite.ppm
    molarmass = p[1]#metabolite.molarmass
    T2b = p[2]#metabolite.T2
    k0 = p[3]#metabolite.k0
    kb = p[4]#metabolite.kb
    GM = p[5]#metabolite.concentration  

    # Water Pool A
    R1A = 1/1.2#1/3
    R2A = 1/0.04#2
    dwA = 0
    
    #CEST pool B
    fB = molarmass * GM
    kBA = k0 + kb * 10 ** (pH - 14)
    dwB = ppm
    R2B = 1/T2b
    R1B = 1
    
    #Sequence parameters
    Zi = 1
    tp = sattime
    
 
    
    #Start functions 
    w_ref = 2 * np.pi * 42.577478518 * B0
    gamma = 267.5153
    w1 = B1 * gamma
    da = (x - dwA)*w_ref
    theta = np.arctan(w1/da)
    
    Reff = R1A * np.cos(theta)**2 + R2A * np.sin(theta)**2
    
    Rex = Rex_Lorentz(da,w1,(x-dwB)*w_ref,fB,kBA,R2B)
    
    R1obs = R1A
    
    R1p = Reff + Rex
    
    Pz = np.cos(theta)
    Pzeff = np.cos(theta)
    
    Zss = np.cos(theta) * Pz * R1obs / R1p
    
    return (Pz * Pzeff * Zi - Zss) * np.exp(-(R1p * tp)) + Zss - y

#-----------------------------------------------------------------------------
#Run simulations from CESTfunctions
    
amountpools = 2

metabolite = cr
metabolitec = glu
metabolited = pcr

B0 = 7
B1 = 2.95
sattime = 1.5
pH = 7

MT = 0


frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym = cest.simdata(
    metabolite, metabolitec, metabolited, B1, B0, sattime, pH, amountpools, MT)


# frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym = cest.simdata(
#     0, 1, 2, 4.85E-6, 7, 0.245, 7, amountpools) #glutamate

# frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym = cest.simdata(
#     2, 1, 2, 1.981E-6, 7, 0.54, 7, amountpools) #creatine

#------------------------------------------------------------------------------
#Use real data

# file = "C:\\Users\\jaimy\\Documents\\Python Scripts\\Master Onderzoek\\ReadData\\Data\\OvernightCEST_Cr_510204080_25062020\\CSV\\Cr_2p95uT_1500ms_10mM.csv"

# frequency, Z = np.loadtxt(file, delimiter=',', usecols=(0, 1), unpack=True)
# frequency = frequency/(42.577478518 * 7)

# ppmb = cr.ppm


#------------------------------------------------------------------------------
#Make arrays and first guesses

x_array = frequency
y_array = 1 - Z

yfit = y_array

p = np.zeros(6)

p[0] = ppmb
p[1] = cr.molarmass
p[2] = 5E-3
p[3] = 0
p[4] = 5000
p[5] = 10E-3

boundsmin = [ppmb-1E-12, p[1]-1E-12, 0, -5, 50, p[5]-1E-12]
boundsmax = [ppmb+1E-12, p[1]+1E-12, 15E-3, 5, 1E4, p[5]+1E-12]

#------------------------------------------------------------------------------
#Optimalisation

counter = 0

while any(t > 0.01 for t in yfit):
    counter += 1
    if counter > 20: #emergency break to avoid infinite loop
        break
    
    popt_2lor = least_squares(zaissfit, p, bounds = (boundsmin, boundsmax), args=(x_array, y_array))
    
    p[0], p[1], p[2], p[3], p[4], p[5] = popt_2lor.x[0:6]
    
    yfit = popt_2lor.fun

#-----------------------------------------------------------------------------
#Plotting

plt.figure()
plt.plot(x_array, 1-zf2.zaissfit(metabolite, B1, sattime), label = "fit")
plt.plot(x_array, y_array, label = "Simulation")
plt.legend()
plt.grid()
plt.xlim(10, -10)
plt.show()

print(p)





