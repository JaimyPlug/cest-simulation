import numpy as np
import matplotlib.pyplot as plt
import CESTfunctions as cest
from metabolites import cr, glu, pcr, mi

xZspec = np.linspace(-10, 10, 1000) #even amount of elements to prevent devision by 0

#------------------------------------------------------------------------------
#functions

def Rex_Lorentz(da,w1,db,fb,kb,r2b):
    ka=kb*fb
     
    REXMAX = ka*w1**2/(da**2+w1**2)*((da-db)**2+(da**2+w1**2)*r2b/kb + r2b*(kb+r2b))
    GAMMA = 2*np.sqrt((kb+r2b)/kb*w1**2+(kb+r2b)**2)
    return REXMAX/((GAMMA/2)**2+db**2)


def zaissfit(metabolite, B1, sattime):
    
    ppm = metabolite.ppm
    molarmass = metabolite.molarmass
    T2b = metabolite.T2
    k0 = metabolite.k0
    kb = metabolite.kb
    GM = metabolite.concentration  

    # Water Pool A
    R1A = 1/1.2#1/3
    R2A = 1/0.04#2
    dwA = 0
    
    #CEST pool B
    fB = molarmass * GM
    kBA = 1100#k0 + kb * 10 ** (pH - 14)
    dwB = ppm
    R2B = 1/T2b
    R1B = 1
    
    #Sequence parameters
    Zi = 1
    tp = sattime
    
 
    
    #Start functions 
    w_ref = 2 * np.pi * 42.577478518 * B0
    gamma = 267.5153
    w1 = B1 * gamma
    da = (xZspec - dwA)*w_ref
    theta = np.arctan(w1/da)
    
    Reff = R1A * np.cos(theta)**2 + R2A * np.sin(theta)**2
    
    Rex = Rex_Lorentz(da,w1,(xZspec-dwB)*w_ref,fB,kBA,R2B)
    
    R1obs = R1A
    
    R1p = Reff + Rex
    
    Pz = np.cos(theta)
    Pzeff = np.cos(theta)
    
    Zss = np.cos(theta) * Pz * R1obs / R1p
    
    return (Pz * Pzeff * Zi - Zss) * np.exp(-(R1p * tp)) + Zss



def maxvals(b1, sat):
    B1 = b1
    tp = sat
    
    Z = zaissfit(metabolite, B1, tp)
                
    ppmindex = np.where(np.abs(xZspec-3.2) < (xZspec[1]-xZspec[0])/2 )[0][0]
    
    minindex = np.min(Z[ppmindex-2:ppmindex+3])
                
    return 1 - minindex



def optimap(concentration):
    sattimes = ["500", "1000", "1500", "2000", "2500"]
    B1s = ["0p5", "1", "1p5", "2", "2p25", "2p5", "2p95", "3p5", "4"]
    
    optimisationmatrix = np.zeros((len(sattimes), len(B1s)))
    
    for i in range(len(B1s)):
        for j in range(len(sattimes)):
            if i > 7:
                if j > 0:
                    maxvalue = 0
                else: 
                    maxvalue = maxvals(i, j)        
            
            elif i > 6:
                if j > 1:
                    maxvalue = 0
                else:
                    maxvalue = maxvals(i,j)
                    
            elif i > 5:
                if j > 2:
                    maxvalue = 0
                else:
                    maxvalue = maxvals(i,j)
                    
            elif i > 4:
                if j > 3:
                    maxvalue = 0
                else:
                    maxvalue = maxvals(i,j)
                    
            else:
                maxvalue = maxvals(i,j)
               
            optimisationmatrix[j,i] = maxvalue
    
    optimisationmatrix = np.flip(optimisationmatrix, axis = 0)        
    optimisationmatrix = optimisationmatrix/np.max(optimisationmatrix) 
    optimisationmatrix = [(i > 0) * i for i in optimisationmatrix]
    
    plt.figure()
    plt.imshow(optimisationmatrix, extent= [0.5, 4, 0.25, 2.75])
    plt.xlabel('B1 [uT]')
    plt.ylabel('saturation time [s]')
    plt.title('Cr ' + concentration)
    plt.colorbar()

#------------------------------------------------------------------------------
#Variables

metabolite      = cr
# metabolitec     = glu
# metabolited     = pcr


B0 = 7
# pH = 7
B1 = 1
sattime = 1.5

#------------------------------------------------------------------------------
#plot Zaissfit

# Zfit = zaissfit(metabolite, B1, sattime)

# #------------------------------------------------------------------------------
# #plot Z-sepctrum

# amountpools = 2
# MT = 0

# frequency, Zbm, ppmb, ppmc, ppmd, ppme, MTRasym = cest.simdata(
#     metabolite, metabolitec, metabolited, B1, B0, sattime, pH, amountpools, MT)



# # S = np.flip(Z)
# # MTRasym = (S - Z)
# # length = len(Z)
# # MTRasym[0:int((length-1)/2)] = 0

# plt.figure()
# # plt.plot(xZspec,Zbm)
# plt.plot(xZspec,Zfit)
# # plt.plot(xZspec, Zbm-Zfit)
# # plt.plot(xZspec,MTRasym)
# plt.xlim(10, -10)
# plt.grid()

#------------------------------------------------------------------------------
#Make optimisation matrix

# for i in ["5mM", "10mM", "20mM", "40mM", "80mM"]:
#     optimap(i)
