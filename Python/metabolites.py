from dataclasses import dataclass

@dataclass
class cr:
    name: str = 'Creatine'                  # Name of metabolite
    shortname: str = 'Cr'
    ppm: int = 2                            # Offset frequency [ppm]
    molarmass: float = 131.13E-3            # Molar mass of metabolite [g/mol]
    T2: float = 7.1E-3                      # T2 time of metabolite [s]
    k0: int = 0                             
    kb: int = 7.81E9
    concentration: float = 10E-3
    
@dataclass
class glu:
    name: str = 'Glutamate'
    shortname: str = 'Glu'
    ppm: float = 3.2
    molarmass: float = 145.11432E-3
    T2: float = 6.9E-3
    k0: int = 2790                          #5500?
    kb: int = 4.5E10
    concentration: float = 10E-3
    
@dataclass
class pcr:
    name: str = 'Phosphocreatine'
    shortname: str = 'pcr'
    ppm: float = 2.64
    molarmass: float = 211.114E-3
    T2: float = 7.8E-3
    k0: int = 11
    kb: int = 1.17E9
    concentration: float = 10E-3
    
@dataclass
class mi:
    name: str = 'Myo-Inositol'
    shortname: str = 'MI'
    ppm: float = 1
    molarmass: float = 180.16E-3
    T2: float = 22.8E-3
    k0: int = 760
    kb: int = 1.39E10
    concentration: float = 10E-3
