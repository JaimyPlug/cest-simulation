#imports necessary to run this script

import numpy as np
import matplotlib.pyplot as plt
import CESTfunctions as cest
from metabolites import cr, glu, pcr, mi

# import matplotlib as mpl

# mpl.rcParams['figure.dpi'] = 300

#-----------------------------------------------------------------------------
#Parameters

B0 = 7
pH = 7
B1 = 2.5  #voorcreatine   #4.85E-6 # voor glutamate
sattime = 2 #voorcreatine   #0.245 # voor glutamate

#-----------------------------------------------------------------------------
#choose metabolite:

metabolite      = cr
metabolitec     = glu
metabolited     = pcr


metabolite.concentration = 40E-3
#-----------------------------------------------------------------------------
#Z-spectrum and MTRasym

amountpools = 2
MT = 0

frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym = cest.simdata(
    metabolite, metabolitec, metabolited, B1, B0, sattime, pH, amountpools, MT)


#-----------------------------------------------------------------------------
#Plot Z and MTRasym
    
plt.figure()
plt.plot(frequency, Z)
# plt.plot(frequency, MTRasym)
plt.xlim([10,-10])
plt.ylim([0,1])
plt.grid()
plt.xlabel('Frequency [ppm]')
plt.ylabel('Z/Z0')
# plt.gcf().subplots_adjust(bottom=0.15)
plt.show()


#-----------------------------------------------------------------------------
#Optimisation map

# b1sweep = np.linspace(0.1, 4, 40)
# b1sweep = b1sweep * 10 ** -6
# satsweep = np.linspace(0.1, 2, 20)

# matrix = cest.optimap(metabolite, b1sweep, B0, satsweep, pH, MT)
# matrix = np.flip(matrix, axis = 0)
# matrix = matrix/np.max(matrix)

# plt.figure()
# plt.imshow(matrix, cmap=plt.cm.magma, extent=
#             [b1sweep[0]*10**6, b1sweep[len(b1sweep)-1]*10**6, satsweep[0], satsweep[len(satsweep)-1]])
# plt.xlabel('B1 [uT]')
# plt.ylabel('saturation time [s]')
# # plt.title('Creatine')
# plt.colorbar()


#-----------------------------------------------------------------------------
#Contribution Function

# frequency, FOSnorm, FOS = cest.contribution(B1, B0, sattime, pH, MT)

# width = frequency[1] - frequency[0]

# plt.figure()
# plt.bar(frequency[525:750], 100*FOSnorm[0], width = width, label = 'Glutamate', color = '#72D056')
# plt.bar(frequency[525:750], 100*FOSnorm[1], bottom = 100*FOSnorm[0], width = width, label = 'Creatine', color = '#404588')
# plt.xlim([4.99,0.5])
# plt.ylim([0,100])
# plt.ylabel('Contribution [%]')
# plt.xlabel('Frequency [ppm]')
# plt.legend(loc = 'lower left')
# plt.gcf().subplots_adjust(bottom=0.15)
# plt.show()

