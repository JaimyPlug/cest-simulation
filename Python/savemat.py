import numpy as np
import scipy.io as sio

matrix = np.arange(1,101,1).reshape((5,5,4))

sio.savemat('test.mat', {'masks':matrix})

mat_contents = sio.loadmat('test.mat')

print(mat_contents['masks'] == matrix)