function [pHsweep, SBPsignal] = phsweep(B1strength, B0, sattime)
    metaname = ["Glutamate", "Myo-Inositol", "Creatine", "Phosphocreatine"];
    SBPsignal = zeros(4, 11);
    pHsweep = 6.5:0.1:7.5;
    dummy = 1;
    
    for ph = pHsweep
        [frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym] = fivepoolmodel(B1strength, B0, sattime, ph);
                
        ppmplaceb = find(abs(frequency-ppmb) < 0.001);
        ppmplacec = find(abs(frequency-ppmc) < 0.001);
        ppmplaced = find(abs(frequency-ppmd) < 0.001);
        ppmplacee = find(abs(frequency-ppme) < 0.001);
        
        SBPsignal(1, dummy) = MTRasym(ppmplaceb);
        SBPsignal(3, dummy) = MTRasym(ppmplacec);
        SBPsignal(4, dummy) = MTRasym(ppmplaced);
        SBPsignal(2, dummy) = MTRasym(ppmplacee);
        
        dummy = dummy + 1;
    end
    
    SBPsignalNorm = SBPsignal./sum(SBPsignal);
    bar(pHsweep, 100*SBPsignalNorm', 'stacked')
    title("Saturation time of " + sattime + " s, B1 = " + 1000000*B1strength + "uT")
    ylim([0, 100])
    xlim([6.45, 7.55])
    xlabel('pH')
    ylabel('Normalized contribution [%]')
    legend({metaname(1),metaname(2),metaname(3),metaname(4)},'Location','eastoutside')
end