function [frequency, Z, ppm, MTRasym] = twopoolmodelxl(metabolite, concentration, B1strength, B0, sattime, pH)
    Properties = [2.88, 180.156E-3, 6.9E-3, 4750, 1470, 2.89E10, 1E-3;      %Glc
                  1, 180.16E-3, 22.8E-3, 2090, 760, 1.39E10, 5.9E-3;        %MI
                  2, 131.13E-3, 7.1E-3, 810, 0, 7.81E9, 8.4E-3;             %Cr
                  2.64, 211.114E-3, 7.8E-3, 126, 11, 1.17E9, 8.4E-3;        %PCr
                  2.91, 103.12E-3, 17.2E-3, 6900, 1030, 5.68E10, 1.5E-3;    %Gaba
                  3.18, 125.15E-3, 10E-3, 49600, 10970, 3.6E11, 2.1E-3;     %Tau, unknown T2
                  3.18, 146.14E-3, 13.8E-3, 22880, 7880, 1.31E11, 3E-3;     %Gln
                  3.2, 145.11432E-3, 6.9E-3, 7480, 2790, 4.5E10, 11.5E-3;   %Glu
                  3.5, 150E-3, 15E-3, 0, 0, 2.22E8, 73E-3                   %mAmides, unknown molar mass and T2
                  ];  %ppm, molar mass, T2, exchange, k0, kb, in-vivo concentration
    
    ppm = Properties(metabolite, 1);                    %Resonance frequency of metabolite

    omegaA = 0;                                         %Resonance frequency of water
    omegaB = 2 * pi * 42.577478518 * B0 * ppm;          %Resonance frequency of metabolite in rad/s 
    T1a = 1.2;                                          %T1 relaxation time water
    T2a = 0.04;                                         %T2 relaxation time water
    T1b = 1;                                            %T1 relaxation time metabolite
    T2b = Properties(metabolite, 3);                    %T2 relaxation time metabolite
    if concentration == 0
        MB = Properties(metabolite, 7) * Properties(metabolite, 2);     %Mass balance
    else
        MB = concentration * Properties(metabolite, 2); 
    end
    Cb = Properties(metabolite, 5) + Properties(metabolite, 6)*10^(pH-14); %Exchange rate
    B1pulse = 42.577478518E6 * B1strength;              %RF intensity
    maxfreq = 42.577478518 * B0 * 10;                   %Max range for plot, 10 is ppm! So 42.5 is right
    frequency = linspace(-maxfreq, maxfreq, 1001);      %Define frequency array with 1001 points
    
    MOa = 1;
    MOb = MB;
    
    Ca = MB * Cb;
    R1a = 1/T1a;
    R2a = 1/T2a;
    R1b = 1/T1b;
    R2b = 1/T2b;
    
    omega1 = 2 * pi * B1pulse;
    
    
    frequency = 2 * pi * frequency;
    Z = zeros(1,1001);
    f = 1;
    M0 = [0, 0, MOa, 0, 0, MOb]';

    for omega = frequency
       A = [-Ca - R2a, omegaA - omega, 0, Cb, 0, 0; 
           omega - omegaA, -Ca - R2a, omega1, 0, Cb, 0;
           0, -omega1, -Ca - R1a, 0, 0, Cb;
           Ca, 0, 0, -Cb - R2b, omegaB - omega, 0;
           0, Ca, 0, omega - omegaB, -Cb - R2b, omega1;
           0, 0, Ca, 0, -omega1, -Cb - R1b];
    
        b = [0, 0, MOa/T1a, 0, 0, MOb/T1b]';
    
        X = A\b;
        
        M =  expm(A * sattime) * (M0 + X) - X;
        
        Z(f) = M(3) + M(6);
        
        f = f + 1;
    end
    
    frequency = frequency/(2*pi*42.577478518 * B0);
    
    %make MTRasym 
    S = flip(Z);
    MTRasym = (S - Z);
    lengte = length(Z);
    MTRasym(1:(lengte+1)/2) = 0;
end