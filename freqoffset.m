function [frequency, FOS] = freqoffset(B1strength, B0, sattime, pH)
    metaname = ["Glc", "MI", "Cr", "PCr", "GABA", "Tau", "Gln", "Glu", "mAmides"];
    FOS = zeros(9, 1001);
    concentration = 0;
    
    for meta = 1:1:9
        [frequency, Z, ppm, MTRasym] = twopoolmodelxl(meta, concentration, B1strength, B0, sattime, pH);
        FOS(meta, :) = MTRasym;
    end
    
    FOSnorm = FOS./sum(FOS);
    FOSplot = FOSnorm(:, 501:901);
    bar(frequency(501:901), 100*FOSplot', 'stacked')
    ylim([0, 100])
    title("Saturation time of " + sattime + " s, B1 = " + 1000000*B1strength + "uT")
    xlabel('Frequency offset [ppm]')
    ylabel('Normalized contribution [%]')
    legend({metaname(1),metaname(2),metaname(3),metaname(4),metaname(5),metaname(6),metaname(7),metaname(8),metaname(9)},'Location','eastoutside')
    set(gca, 'XDir','reverse')
end