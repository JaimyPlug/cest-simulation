function [frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym] = simdata(metabolite, metabolitec, metabolited, concentration, B1strength, B0, sattime, pH, AmountPools)
    ppmc = 0;
    ppmd = 0;
    ppme = 0;
    
    if AmountPools == 2
        [frequency, Z, ppmb, MTRasym] = twopoolmodel(metabolite, concentration, B1strength, B0, sattime, pH);
    elseif AmountPools == 3
        [frequency, Z, ppmb, ppmc, MTRasym] = threepoolmodel(metabolite, metabolitec, B1strength, B0, sattime, pH);
    elseif AmountPools == 4
        [frequency, Z, ppmb, ppmc, ppmd, MTRasym] = fourpoolmodel(metabolite, metabolitec, metabolited, B1strength, B0, sattime, pH);
    elseif AmountPools == 5
        [frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym] = fivepoolmodel(B1strength, B0, sattime, pH);
    else
        msg = 'AmountPools must be 2, 3, 4 or 5.';
        error(msg)
    end
end