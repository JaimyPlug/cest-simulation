function [frequency, Z, ppmb, ppmc, ppmd, ppme, MTRasym] = fivepoolmodel(B1strength, B0, sattime, pH)
    Properties = [3.2, 145.11432E-3, 6.9E-3, 7480, 2790, 4.5E10, 11.5E-3;
             1, 180.16E-3, 22.8E-3, 2090, 760, 1.39E10, 5.9E-3;
             2, 131.13E-3, 7.1E-3, 810, 0, 7.81E9, 8.4E-3;
             2.64, 211.114E-3, 7.8E-3, 126, 11, 1.17E9, 8.4E-3];  %ppm, molar mass, T2, exchange, k0, kb, concentration GM
    
    metaboliteb = 1;                                    %Glutamate
    metabolitec = 2;                                    %Myo-Inositol
    metabolited = 3;                                    %Creatine
    metabolitee = 4;                                    %Phosphocreatine
         
    ppmb = Properties(metaboliteb, 1);                  %Resonance frequency of metabolite B
    ppmc = Properties(metabolitec, 1);                  %Resonance frequency of metabolite C
    ppmd = Properties(metabolited, 1);                  %Resonance frequency of metabolite D
    ppme = Properties(metabolitee, 1);                  %Resonance frequency of metabolite E

    omegaA = 0;                                         %Resonance frequency of water
    omegaB = 2 * pi * 42.577478518 * B0 * ppmb;         %Resonance frequency of metabolite B in Hz 
    omegaC = 2 * pi * 42.577478518 * B0 * ppmc;         %Resonance frequency of metabolite C in Hz
    omegaD = 2 * pi * 42.577478518 * B0 * ppmd;         %Resonance frequency of metabolite D in Hz
    omegaE = 2 * pi * 42.577478518 * B0 * ppme;         %Resonance frequency of metabolite E in Hz
    T1a = 1.2;                                          %T1 relaxation time water
    T2a = 0.04;                                         %T2 relaxation time water
    T1b = 1;                                            %T1 relaxation time metabolite B
    T2b = Properties(metaboliteb, 3);                   %T2 relaxation time metabolite B
    T1c = 1;                                            %T1 relaxation time metabolite C
    T2c = Properties(metabolitec, 3);                   %T2 relaxation time metabolite C
    T1d = 1;                                            %T1 relaxation time metabolite D
    T1e = 1;                                            %T1 relaxation time metabolite D
    T2e = Properties(metabolitee, 3);                   %T2 relaxation time metabolite E
    T2d = Properties(metabolited, 3);                   %T2 relaxation time metabolite E
    MBb = Properties(metaboliteb, 7) * Properties(metaboliteb, 2);  %Mass balance
    MBc = Properties(metabolitec, 7) * Properties(metabolitec, 2);  %Mass balance
    MBd = Properties(metabolited, 7) * Properties(metabolited, 2);  %Mass balance
    MBe = Properties(metabolitee, 7) * Properties(metabolitee, 2);  %Mass balance
    Cb = Properties(metaboliteb, 5) + Properties(metaboliteb, 6)*10^(pH-14); %Exchange rate B
    Cc = Properties(metabolitec, 5) + Properties(metabolitec, 6)*10^(pH-14); %Exchange rate C
    Cd = Properties(metabolited, 5) + Properties(metabolited, 6)*10^(pH-14); %Exchange rate D
    Ce = Properties(metabolitee, 5) + Properties(metabolitee, 6)*10^(pH-14); %Exchange rate E
    B1pulse = 42.577478518E6 * B1strength;              %RF intensity
    maxfreq = 42.577478518 * B0 * 10;                   %Max range for plot, 10 is ppm! So 42.5 is right
    frequency = linspace(-maxfreq, maxfreq, 1001);      %Define frequency array with 1001 points
    
    MOa = 1;
    MOb = MBb;
    MOc = MBc;
    MOd = MBd;
    MOe = MBe;
    
    Cab = MBb * Cb;
    Cac = MBc * Cc;
    Cad = MBd * Cd;
    Cae = MBe * Ce;
    Ca = Cab + Cac + Cad + Cae;
    R1a = 1/T1a;
    R2a = 1/T2a;
    R1b = 1/T1b;
    R2b = 1/T2b;
    R1c = 1/T1c;
    R2c = 1/T2c;
    R1d = 1/T1d;
    R2d = 1/T2d;
    R1e = 1/T1e;
    R2e = 1/T2e;
    
    k1a = R1a + Ca;
    k2a = R2a + Ca;
    k1b = R1b + Cb;
    k2b = R2b + Cb;
    k1c = R1c + Cc;
    k2c = R2c + Cc;
    k1d = R1d + Cd;
    k2d = R2d + Cd;
    k1e = R1e + Ce;
    k2e = R2e + Ce;
    
    omega1 = 2 * pi * B1pulse;
    
    frequency = 2 * pi * frequency;
    Z = zeros(1,1001);
    f = 1;
    M0 = [0, 0, MOa, 0, 0, MOb, 0, 0, MOc, 0, 0, MOd, 0, 0, MOe]';
    
    for omega = frequency
       A = [-k2a, omegaA - omega, 0, Cb, 0, 0, Cc, 0, 0, Cd, 0, 0, Ce, 0, 0; 
           omega - omegaA, -k2a, omega1, 0, Cb, 0, 0, Cc, 0, 0, Cd, 0, 0, Ce, 0;
           0, -omega1, -k1a, 0, 0, Cb, 0, 0, Cc, 0, 0, Cd, 0, 0, Ce;
           Cab, 0, 0, -k2b, omegaB - omega, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
           0, Cab, 0, omega - omegaB, -k2b, omega1, 0, 0, 0, 0, 0, 0, 0, 0, 0;
           0, 0, Cab, 0, -omega1, -k1b, 0, 0, 0, 0, 0, 0, 0, 0, 0;
           Cac, 0, 0, 0, 0, 0, -k2c, omegaC - omega, 0, 0, 0, 0, 0, 0, 0;
           0, Cac, 0, 0, 0, 0, omega - omegaC, -k2c, omega1, 0, 0, 0, 0, 0, 0;
           0, 0, Cac, 0, 0, 0, 0, -omega1, -k1c, 0, 0, 0, 0, 0, 0;
           Cad, 0, 0, 0, 0, 0, 0, 0, 0, -k2d, omegaD - omega, 0, 0, 0, 0;
           0, Cad, 0, 0, 0, 0, 0, 0, 0, omega - omegaD, -k2d, omega1, 0, 0, 0;
           0, 0, Cad, 0, 0, 0, 0, 0, 0, 0, -omega1, -k1d, 0, 0, 0;
           Cae, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -k2e, omegaE - omega, 0;
           0, Cae, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, omega - omegaE, -k2e, omega1;
           0, 0, Cae, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -omega1, -k1e];
    
        b = [0, 0, MOa/T1a, 0, 0, MOb/T1b, 0, 0, MOc/T1c, 0, 0, MOd/T1d, 0, 0, MOe/T1e]';
    
        X = A\b;
        
        M =  expm(A * sattime) * (M0 + X) - X;
        
        Z(f) = M(3) + M(6) + M(9) + M(12) + M(15);
        
        f = f + 1;
    end
    
    frequency = frequency/(2*pi*42.577478518 * B0);
    
    %make MTRasym
    S = flip(Z);
    MTRasym = (S - Z);
    lengte = length(Z);
    MTRasym(1:(lengte+1)/2) = 0;
end